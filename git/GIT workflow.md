## The __master__ branch should always represent the stable version of the project

### From __master__ branch do:

    git checkout -b develop

#### This will create a branch on which you can work on before merging in __master__ when stable enough.
#### From develop you should create a different branch for every feature you're working on for example

    git checkout -b develop_adding_custom_TextView

#### When a feature is ready, you can then follow this process:

    git add <file-name-1> <file-name-2> <file-name-3> ...
    git commit -m "message"

or
    git add .
    git commit -m "message"

or

    git commit -a -m "message"

or
    
    git commit -am "message"

#### At this point changes are ready to be pushed, the best way to push without doing any damage since there may be conflicts is the following

    git checkout develop #go back to develop
    git pull # fetch edited code from develop so to merge with up-to-date project THIS IS IMPORTANT ELSE YOU WILL THEN PUSH CODE MISSING FEATURES
    git checkout -b tmp #create a temporary branch
    git merge develop_adding_custom_TextView # merge the code from develop_adding_custom_TextView IN the tmp branch
    git status #check for conflicts
    git mergetool #tool to solve conflicts, or use in AndroidStudio/IntelliJ/ETC the solve conflict tool
    #at this point the changes should be automatically committed
    git checkout develop # go back to develop
    git merge tmp # since you already solved the conflicts in tmp you won't get any in develop
    git push # the merged code will then be pushed to the remote repository
    
### This procedure will make it so that you won't create problems in the develop branch (even if locally) since it is a bit problematic to remove commits or merge stuff when done, so if you encounter any problems you can just delete the tmp branch and start again

    git branch -d tmp
    
