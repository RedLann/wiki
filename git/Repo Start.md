# How to start a git repo

### Cloning a repo
```bash
#this will clone the repo in <current_working_directory>/<reponame>
git clone <url>
#this will clone the repo in the current working directory
git clone <url> .
```

### Repo from empty directory, assuming you already created a repo on Git[Hub/Lab etc] site
```bash
cd my_empty_repo_dir
echo "test" >> CommitTest.txt
git add CommitTest.txt # or git add .
git commit -m "message"
git remote add origin <url>
#push in branch 'master' (creating it)
git push -u origin master
```


### Repo from existing files/dirs, assuming you already created a repo on Git[Hub/Lab etc] site

```bash
git init
# add all files
git add . 
git commit -m 'message'
git remote add origin <url> #repo url from GitHub/Lab
#push in branch 'master' (creating it)
git push -u origin master```
