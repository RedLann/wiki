#!/bin/bash

echo "Please, enter your username, it will be added to 'sudo' and 'docker' groups during the process."
read USERNAME

if [ -z "$USERNAME" ] ; then
	echo "Exiting... Done."
	exit
else
	echo "Adding user to 'sudo' group..."
	sudo usermod -aG sudo $USERNAME

	echo "Initializing script..."
	sudo apt-get update
	sudo apt-get -y upgrade

	echo "Installing Nginx..."
	sudo apt-get install -y nginx

	echo "Adding rule to 'ufw' firewall"
	sudo ufw allow 'Nginx HTTP'


	echo "Installing PHP 7 and its components: [cli,common,fpm,curl,gd, intl,zip,xsl,mbstring,mysql]..."
	sudo apt-get install -y php7.0-cli
	sudo apt-get install -y php7.0-common
	sudo apt-get install -y php7.0
	sudo apt-get install -y php7.0-fpm
	sudo apt-get install -y php7.0-curl
	sudo apt-get install -y php7.0-gd
	sudo apt-get install -y php7.0-intl
	sudo apt-get install -y php7.0-zip
	sudo apt-get install -y php7.0-xsl
	sudo apt-get install -y php7.0-mbstring
	# required for installing xdebug
	sudo apt-get install -y php7.0-dev

	echo "Configuring PHP..."
	sudo sed -i s/\;cgi\.fix_pathinfo\s*\=\s*1/cgi.fix_pathinfo\=0/ /etc/php/7.0/fpm/php.ini

	# Composer
	echo "Installing Composer..."
	curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

	echo "Installing Composer Asset plugin..."
	sudo composer global require "fxp/composer-asset-plugin:~1.1"

	echo "Restarting PHP, Nginx and MySQL services..."
	sudo /etc/init.d/php7.0-fpm restart
	sudo /etc/init.d/nginx restart
	
	echo "Installing MongoDB..."
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
	sudo echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
	sudo apt-get update
	sudo apt-get install -y --allow-unauthenticated mongodb-org
	sudo echo '[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/mongodb.service
	sudo systemctl start mongodb
	sudo systemctl status mongodb
	sudo systemctl enable mongodb

	echo "Installing GIT..."
	sudo apt-get install -y git

	echo "Removing unneeded dependencies"
	sudo apt-get autoremove
fi