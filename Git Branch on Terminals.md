__Working for AndroidStudio terminal__

![AndroidStudio Terminal](img/studio-terminal.png)

__and other terminals (Terminator in screenshot)__

![Terminator](img/terminator.png)

HOW-TO: 1 - Open .bashrc

```sudo nano ~/.bashrc```

2 - Find the following (~#59)

```if [ "$color_prompt" = yes ]; then```

3 - Edit the line __below__ that starts with __PS1=__ in:

```PS1='$(printf "%*s\r%s" $(( COLUMNS+15 )) "\[\033[01;32m\] $(git branch 2>/dev/null | grep '^*' | sed s/..//) \[\033[00m\]" "${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$\n> ")'```

4 - Now type the following command to update .bashrc settings

```source ~/.bashrc```

__EXTRA__ If you use non-default terminals you may need to force command prompt colors, you can do it by uncommenting or adding this line in .bashrc (~#46)

```force_color_prompt=yes```
